import { Component, MouseEventHandler } from 'react';
import axios from 'axios';

export type User = {id:number, username: string, login: string, firstName: string, lastName: string, gender: string, image: string, token: string};

export interface ILoginComponentProps{
    onButtonClick: (user: User) => void;
}

export default class LoginComponent extends Component<ILoginComponentProps>{
    login = '';
    password = '';

    constructor(props: any){
        super(props);
    }

    onClick = () => { Login(this.login, this.password, this.props.onButtonClick)
        /*
        axios({
            method: 'post',
            url: 'auth/login?q=proxy',
            headers: { 'Content-Type': 'application/json' },
            withCredentials: false,
            data: JSON.stringify(
                {
                    username: this.login,
                    password: this.password
                }
            )
        })
        .then((response) => { console.log(response.data); this.props.onButtonClick(response.data); this.login=''; this.password=''; })
        .catch((error) => { console.log(error); this.forceUpdate(); });
        */
    };

    render(){
        const buttonStyle={
            borderStyle: "solid",
            borderColor: "Gray",
            borderWidth: "0.4rem",
            borderRadius: "0.8rem",
            backgroundColor: "DimGray",
            padding: "0.1rem 0.75rem 0.2rem 0.75rem",
            fontWeight: "800",
            color: "GhostWhite"
        };
        return(
            <div style={{borderStyle: "solid", borderColor: "Gray", borderWidth: "0.4rem 0 0.4rem 0", borderRadius: "0.8rem", padding: "0 0.75rem 0.45rem 0.75rem"}}>
                <input type='text' className='input' placeholder='Enter login' name='loginInput' style={{margin: "0 0 0.9rem 0", fontWeight: "600", color: "OrangeRed"}} onChange={(e:any) => {this.login = e.target.value}}/>
                <br />
                <input type='password' className='input' placeholder='Enter password' name='passwordInput' style={{margin: "0 0 0.9rem 0", fontWeight: "600", color: "OrangeRed"}} onChange={(e:any) => {this.password = e.target.value}}/>
                <br />
                <button style={buttonStyle} onClick={() => Login(this.login, this.password, this.props.onButtonClick)}>Войти</button>
            </div>
        );
    }
}

export function Login(login: string, password: string, callback: ((user: User) => void) | undefined){
    if (!callback) throw "callback is undefined";
    axios({
        method: 'post',
        url: 'auth/login?q=proxy',
        headers: { 'Content-Type': 'application/json' },
        withCredentials: false,
        data: JSON.stringify(
            {
                username: login,
                password: password
            }
        )
    })
    .then((response) => { callback(response.data); })
    .catch((error) => { console.log(error); });
};
import React, { createContext, useState } from 'react';
import logo from './logo.svg';
import './App.css';
import GreetingsComponent from './GreetingsComponent';
import LoginComponent from './LoginComponent';
import UsersExampleComponent from './UsersExampleComponent';
import { User } from './LoginComponent';

interface AppState
{
  user: User | undefined
}

const AppContext = createContext({ user: undefined, })

function App() {
  const [user, setData] = useState<User|undefined>();

  const onLogin = (user: User) => {
    setData(user);
  };

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className={user != undefined ? "App-logo" : "App-logo-sm"} alt="logo" />
        {user &&
          <div>
            <GreetingsComponent name={`${user?.firstName} ${user?.lastName}`}/>
            <br />
          </div>
        }
        {!user &&
          <div>
            <LoginComponent onButtonClick={onLogin}/>
            <br/>
            <UsersExampleComponent  onClick={onLogin}/>
          </div>
        }
      </header>
    </div>
  );
}

export default App;

import { Component, MouseEventHandler } from 'react';

export interface IGreetingsComponentProps{
    name: string;
}

export default class GreetingsComponent extends Component<IGreetingsComponentProps>{
    greetText = 'Приветствую тебя';
    render(){
        const buttonStyle={
            borderStyle: "solid",
            borderColor: "Gray",
            borderWidth: "0.4rem",
            borderRadius: "0.8rem",
            backgroundColor: "DimGray",
            padding: "0.75rem",
            fontWeight: "800",
            color: "GhostWhite"
        };
        return(
            <div>
                <div style={{borderStyle: "solid", borderColor: "Gray", borderWidth: "0.4rem 0 0.4rem 0", background: "linear-gradient(to left, transparent, DarkGray 3%, DarkGray 97%, transparent)", padding: "0 0.75rem 0.45rem 0.75rem"}}>
                    <span style={{fontWeight: "600", color: "OrangeRed"}}>
                        {this.greetText}, {this.props.name.length > 0 ? this.props.name : "некто"}
                    </span>
                </div>
            </div>
        );
    }
}